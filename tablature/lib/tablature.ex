defmodule Tablature do
  require Integer

  def parse(mult) do
    tabs = String.split(mult, "\r\n\r\n")
    helper(tabs)
  end

  def helper([h|[]]), do: parse_tab(h)
  def helper([h|t]) do
    parse_tab(h) <> " " <> helper(t)
  end

  def parse_tab(tab) do
    non_numbers = get_non_numbers(tab)
    separators = parse_separators(Enum.at(String.split(tab), 0))
    unders = Enum.map(non_numbers -- separators, fn temp -> {"_",temp} end)

    tab
    |> String.split()
    |> Enum.map(fn line -> parse_line(line) end)
    |> List.flatten
    |> Enum.concat(unders)
    |> Enum.sort_by(fn {_caracter, posicion} -> {posicion} end)
    |> Enum.group_by(fn {_caracter, posicion} -> {posicion} end)
    |> Enum.map(fn {_group, caracter} -> caracter end)
    |> Enum.map(fn line ->
      if length(line) > 1 do
        Enum.reduce(line, "", fn {caracter, _posicion}, counter -> counter <> caracter <> "/" end)
        |> String.trim("/")
      else
        Enum.reduce(line,"", fn {caracter, _posicion}, counter -> counter <> caracter end)
      end
    end)
    |> Enum.join(" ")
  end

  def parse_line(line) do
    note = String.first(line)
    String.codepoints(line)
    |> Enum.with_index()
    |> Enum.filter(fn {caracter, _posicion} -> Regex.match?(~r/[0-9]/, caracter) end)
    |> Enum.map(fn {caracter,posicion} -> {note <> caracter, posicion} end)
  end

  def get_non_numbers(tab) do
    x = String.split(tab)
    |> Enum.map(fn line -> parse_line(line) end)
    |> List.flatten
    |> Enum.sort_by(fn {_caracter, posicion} -> {posicion} end)
    |> Enum.group_by(fn {_caracter, posicion} -> {posicion} end)
    |> Enum.map(fn {group, _caracter} -> group end)

    with_num = Enum.flat_map(x, fn {x} -> [x] end)
    last = Enum.at(Enum.take(with_num, -1), 0)
    without_num = Enum.to_list(3..last)
    |> Enum.filter(fn number -> Integer.is_odd(number) end)

    without_num -- with_num
  end

  def parse_separators(line) do
    String.codepoints(line)
    |> Enum.with_index()
    |> Enum.filter(fn {caracter, _posicion} -> Regex.match?(~r/\|/, caracter) end)
    |> Enum.map(fn {_caracter, posicion} -> posicion end)
  end
end
